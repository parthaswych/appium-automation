package constants;

import java.io.File;

public class Constants {
		
	public static final String CONFIG_PROPERTY_FILENAME = "src/main/resources/config" + File.separator + "Config.properties";
	public static final String UserSMOKE="userSmoke.xls";
	public static final String AdminSMOKE="adminSmoke.xls";
	public static final String dataProviderClass1="dataProviderClass = gci.util.dataproviders.ExcelFileDataProvider.class";
	public static final String  dataProvider = "getDataFromExcelFile";
	public static final String dataProviderClass = "gci.util.dataproviders.ExcelFileDataProvider.class";
	public static final String Login="login.xls";
	public static final String AdminLogin="adminlogin.xls";
	public static final String sendgift="sendgift.xls";
	public static final String userTransactionHistory="userTransactionHistory.xls";
	public static final String errorReport="errorReport.xls";
	public static final String userChangePassword="userChangePassword.xls";
	public static final String userResetPassword="userResetPassword.xls";
	public static final String AdminHome="adminhome.xls";
	
}
