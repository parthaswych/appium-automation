package Pageobject;

import Utilities.Tools;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

import java.util.List;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

public class BasePage extends base{

    protected static final int PAGE_READY_TIMEOUT = 20;  // wait for up to 60 seconds to load the page
    protected static final int PAGE_MAY_APPEAR_TIMEOUT = 10;  // wait for up to 10 seconds to load the page

    protected AndroidDriver driver;

    @AndroidFindBy(className = "UIAKeyboard")
    private AndroidElement keyboard;

    private static final int ELEMENT_READY_TIMEOUT = 30;
    private static final int ELEMENT_READY_TIMEOUT_SHORT = 5;

    BasePage() {}
    BasePage(AndroidDriver driver) {
        this.driver = driver;
    }

    public void hideKeyboardIfVisible() {
        if (keyboard != null) {
            driver.pressKey(new KeyEvent(AndroidKey.ESCAPE));
        }
    }

    public void clickKeyboardBackspace() {
        if (keyboard != null) {
            driver.pressKey(new KeyEvent(AndroidKey.DEL));
        }
    }

    protected boolean isElementEditable(MobileElement element) {
        element.click();
        boolean isShown = driver.isKeyboardShown();
        driver.hideKeyboard();
        return isShown;
    }

    protected void waitForElement(MobileElement element) {
        Tools.waitForElement(driver, element, ELEMENT_READY_TIMEOUT);
    }

    protected void waitForAndClickElement(MobileElement element) {
        Tools.waitForElement(driver, element, ELEMENT_READY_TIMEOUT);
        element.click();
    }

    protected void waitForAndEnterEdit(MobileElement element, String text) {
        Tools.waitForElement(driver, element, ELEMENT_READY_TIMEOUT);
        element.click();
        element.sendKeys(text);
    }

    protected void scroll(MobileElement from, MobileElement to) {
        TouchAction ta = new TouchAction(driver);
        ta.longPress(element(from)).moveTo(element(to)).release().perform();
    }

    public void swipeByElements(MobileElement startElement, MobileElement endElement) {
        int startX = startElement.getLocation().getX() + (startElement.getSize().getWidth() / 2);
        int startY = startElement.getLocation().getY() + (startElement.getSize().getHeight() / 2);

        int endX = endElement.getLocation().getX() + (endElement.getSize().getWidth() / 2);
        int endY = endElement.getLocation().getY() + (endElement.getSize().getHeight() / 2);

        new TouchAction(driver)
                .press(point(startX,startY))
                .waitAction(waitOptions(ofMillis(1000)))
                .moveTo(point(endX, endY))
                .release().perform();
    }

    public void clickTextView(MobileElement parentElement, String elementText) {
        List<MobileElement> items = parentElement.findElements(By.className("android.widget.TextView"));
        for (MobileElement item : items) {
            if (item.getText().equals(elementText)) {
                item.click();
                break;
            }
        }
    }
}
