package Pageobject;

import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Homepageobject {
	
	public Homepageobject(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		
	}
	
	
	@AndroidFindBy(xpath="//android.widget.EditText[@index='1']")
	public WebElement mobilenumber;
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='GET STARTED']")
	public WebElement getstartedbutton;

	@AndroidFindBy(id="com.goswych.swychapp:id/edit_otp_alert_verification_code")
	private AndroidElement otpTextbox;

	@AndroidFindBy(id="com.goswych.swychapp:id/button_alert_resend")
	private AndroidElement resendBtn;

	@AndroidFindBy(id="com.goswych.swychapp:id/button_alert_submit")
	private AndroidElement submitBtn;

	@AndroidFindBy(id="com.goswych.swychapp:id/country_code_spinner")
	private AndroidElement countryCodeSpinner;

	@AndroidFindBy(xpath="//android.widget.TextView[@text='+971 UAE']")
	private AndroidElement countryCodeUAE;


	
	public WebElement mobilenumber() {
		System.out.println("On the Home page - Find And Enter the mobile number\n");
		return mobilenumber;
	}
	
	public WebElement getstartedbutton() {
		System.out.println("On the Home page - Find And Click Get Started\n");
		return getstartedbutton;
	}

	public WebElement getOTPtextbox() {
		System.out.println("On the Home page - Find and click OTP box\n");
		return otpTextbox;
	}

	public WebElement getResendBtn() {
		System.out.println("On the Home page - Find and click Resend\n");
		return resendBtn;
	}

	public WebElement getSubmitBtn() {
		System.out.println("On the Home page - Find and click Submit\n");
		return submitBtn;
	}

	public WebElement getCountryCodeSpinner() {
		System.out.println("On the Home page - Find and click CountryCodeSpinner\n");
		return countryCodeSpinner;
	}

	public WebElement getCountryCodeUAE() {
		System.out.println("On the Home page - Find and click CountryCodeUAE\n");
		return countryCodeUAE;
	}



}
