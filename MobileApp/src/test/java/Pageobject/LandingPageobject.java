package Pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LandingPageobject {
	public LandingPageobject(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	
		
	}
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='BROWSE ALL CARDS']")
	public WebElement browseallcards;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MY CARDS']")
	public WebElement maycards;
	
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='SEND']")
	public WebElement send;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='BUY']")
	public WebElement buy;

	@AndroidFindBy(id = "com.goswych.swychapp:id/image_hamburger")
	public WebElement hamburgerBtn;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='LOG OUT']")
	public WebElement logoutBtn;

	@AndroidFindBy(id = "com.goswych.swychapp:id/RecyclerView")
	public WebElement slidingSideBar;

	@AndroidFindBy(id = "com.goswych.swychapp:id/button_message_yes")
	public WebElement logoutYes;

	@AndroidFindBy(id = "com.goswych.swychapp:id/img_Avatar")
	public WebElement avatarImg;

	@AndroidFindBy(id = "com.goswych.swychapp:id/fragment_settings_edit_password")
	public WebElement changePwdClickable;

	@AndroidFindBy(id = "com.goswych.swychapp:id/fragment_settings_edit_password_edittext_oldpassword")
	public WebElement oldPwdTextBox;

	@AndroidFindBy(id = "com.goswych.swychapp:id/fragment_settings_edit_password_edittext_newpassword")
	public WebElement newPwdTextBox;

	@AndroidFindBy(id = "com.goswych.swychapp:id/fragment_settings_edit_password_edittext_confirmpassword")
	public WebElement confirmPwdTextBox;

	@AndroidFindBy(id = "com.goswych.swychapp:id/fragment_settings_edit_password_savebutton")
	public WebElement pwdEditSaveBtn;

	@AndroidFindBy(id = "com.goswych.swychapp:id/label_message_introduction")
	public WebElement popupMessage;

	@AndroidFindBy(id = "com.goswych.swychapp:id/button_message_ok")
	public WebElement messageOKBtn;

	@AndroidFindBy(id = "com.goswych.swychapp:id/fragment_settings_sign_out")
	public WebElement signoutBtn;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='SETTINGS']")
	public WebElement settingsBtn;

	@AndroidFindBy(xpath="//android.widget.Button[@text='Next']")
	public WebElement nextBtn;

	@AndroidFindBy(xpath="//android.widget.Button[@text='End']")
	public WebElement endBtn;

	@AndroidFindBy(xpath="//android.widget.Button[@text='Skip']")
	public WebElement skipBtn;
	
	public WebElement maycards() {
		System.out.println("On the Page after login page - Find MY CARDS\n");
		return maycards;
	}
	
	public WebElement send() {
		System.out.println("On the Page after login page - Find  SEND\n");
		return send;
	}
	public WebElement buy() {
		System.out.println("On the Page after login page - Find  BUY\n");
		return buy;
	}
	public WebElement browseallcards() {
		System.out.println("Click Browse All cards\n");
		return browseallcards;
	}

	public WebElement getHamburgerBtn() {
		System.out.println("Click hamburger button\n");

		return hamburgerBtn;
	}

	public WebElement getLogoutBtn() {
		System.out.println("Click logout\n");

		return logoutBtn;
	}

	public WebElement getSlidingSideBar() {
		System.out.println("Find hamburger menu\n");

		return slidingSideBar;
	}

	public WebElement getLogoutYes() {
		System.out.println("Click Yes in popup\n");
		return logoutYes;
	}

	public WebElement getAvatarImg() {
		System.out.println("Click avatar image\n");
		return avatarImg;
	}

	public WebElement getChangePwdClickable(){
		System.out.println("Click Change Password\n");
		return changePwdClickable;
	}

	public WebElement getOldPwdTextBox(){
		System.out.println("Find Old Password text box\n");
		return oldPwdTextBox;
	}

	public WebElement getnewPwdTextBox(){
		System.out.println("Find New Password text box\n");
		return newPwdTextBox;
	}

	public WebElement getConfirmPwdTextBox(){
		System.out.println("Find Confirm Password text box\n");
		return confirmPwdTextBox;
	}

	public WebElement getPwdEditSaveBtn(){
		System.out.println("Click Save button\n");
		return pwdEditSaveBtn;
	}

	public WebElement getPopupMessage(){
		System.out.println("Find popup message\n");
		return popupMessage;
	}

	public WebElement getMessageOKBtn(){
		System.out.println("Find Change Password OK button\n");
		return messageOKBtn;
	}

	public WebElement getSignoutBtn(){
		System.out.println("Find Sign Out button\n");
		return signoutBtn;
	}

	public WebElement getSettingsBtn(){
		System.out.println("Find Settings button in hamburger menu\n");
		return settingsBtn;
	}

	public WebElement getNextBtn() {
		System.out.println("On Take A Tour - Find and click Next\n");
		return nextBtn;
	}

	public WebElement getEndBtn() {
		System.out.println("On Take A Tour - Find and click End\n");
		return endBtn;
	}

	public WebElement getSkipBtn() {
		System.out.println("On Take A Tour - Find and click Skip\n");
		return skipBtn;
	}
}
