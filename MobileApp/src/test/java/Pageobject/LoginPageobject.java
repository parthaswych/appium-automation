package Pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginPageobject {
	
	
	public LoginPageobject(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		
	}
	
	@AndroidFindBy(id = "com.goswych.swychapp:id/edit_password")
	public WebElement password;
	
	@AndroidFindBy(id = "com.goswych.swychapp:id/btnSignupLogin")
	public WebElement loginbutton;

	@AndroidFindBy(id = "com.goswych.swychapp:id/label_forget_password")
	public WebElement forgotPasswordLink;

	@AndroidFindBy(id = "com.goswych.swychapp:id/dialog_forget_password_phonenumber_savebutton")
	public WebElement submitBtn;

	@AndroidFindBy(id="com.goswych.swychapp:id/edit_otp_alert_verification_code")
	private AndroidElement otpTextbox;

	@AndroidFindBy(id="com.goswych.swychapp:id/button_alert_submit")
	private AndroidElement otpSubmitBtn;

	@AndroidFindBy(id="com.goswych.swychapp:id/dialog_forget_password_edittext_newpassword")
	private AndroidElement newPwdBox;

	@AndroidFindBy(id="com.goswych.swychapp:id/dialog_forget_password_edittext_confirmpassword")
	private AndroidElement confirmPwdBox;

	@AndroidFindBy(id="com.goswych.swychapp:id/dialog_forget_password_applybutton")
	private AndroidElement forgotPwdApplyBtn;

	@AndroidFindBy(id="com.goswych.swychapp:id/button_message_ok")
	private AndroidElement resetPwdOkBtn;
	
	
	public WebElement password() {
		System.out.println("Find And Enter the Password\n");
		return password;
	}
	
	public WebElement loginbutton() {
		System.out.println("Find And Click Login Button\n");
		return loginbutton;
	
	}

	public WebElement getForgotPasswordLink() {
		System.out.println("Click on Forgot Password\n");
		return forgotPasswordLink;
	}

	public WebElement getSubmitBtn() {
		System.out.println("Click on Submit button\n");
		return submitBtn;
	}

	public WebElement getotpTextbox() {
		System.out.println("Find and click OTP box\n");
		return otpTextbox;
	}

	public WebElement getotpSubmitBtn() {
		System.out.println("Click on OTP Submit button\n");
		return otpSubmitBtn;
	}

	public WebElement getnewPwdBox() {
		System.out.println("Find newPwdBox\n");
		return newPwdBox;
	}

	public WebElement getconfirmPwdBox() {
		System.out.println("Find confirmPwdBox\n");
		return confirmPwdBox;
	}

	public WebElement getforgotPwdApplyBtn() {
		System.out.println("Find and click on Apply button\n");
		return forgotPwdApplyBtn;
	}

	public WebElement getresetPwdOKBtn() {
		System.out.println("Click on OK button\n");
		return resetPwdOkBtn;
	}
}
