package Pageobject;

import Utilities.Tools;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessagingListPage extends BasePage {

    private static final String REGEXP_OTP_PATTERN = "(Your Swych verification code is\\:)\\s(\\d{6})";
    protected static final int PAGE_READY_TIMEOUT = 50;  // wait for up to 60 seconds to load the page

    @WithTimeout(time = PAGE_READY_TIMEOUT, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(id = "android:id/list")  //Nexus 5 messaging
    //@AndroidFindBy(id = "com.samsung.android.messaging:id/conversationRecycleListView")  // Samsung device
    private AndroidElement listMessage;


    public MessagingListPage() {}

    public MessagingListPage(AndroidDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        Tools.waitForElement(driver, this.listMessage, PAGE_READY_TIMEOUT);
    }

    public String getOtp() {
        //List<MobileElement> items = listMessage.findElements(By.id("com.android.mms:id/subject"));
        //Nexus 5
        List<MobileElement> items = listMessage.findElements(By.id("com.google.android.apps.messaging:id/conversation_snippet"));
        //J7
        //List<MobileElement> items = listMessage.findElements(By.id("com.samsung.android.messaging:id/text_content"));
        Pattern pattern = Pattern.compile(REGEXP_OTP_PATTERN);
        for (MobileElement item : items) {
            String t = item.getText();
            Matcher matcher = pattern.matcher(t);
            if (matcher.find()) {
                return matcher.group(2);
            }
        }
        return null;
    }
}
