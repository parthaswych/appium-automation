package Pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OrderConfirmation {
	public OrderConfirmation(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	
		
	}
	
	@AndroidFindBy(xpath="//android.widget.TextView[@index='0']")
	public WebElement message;
	
	public WebElement message() {
		
		System.out.println("The message is "+message);
		System.out.println("Validate the Thank you message on the order confirmation page\n");
		//String msg = message.getText().substring(0, 25);
		String expectedmessage="Thank you for your Order.";
		/*
		if (msg.equals(expectedmessage)) {
			System.out.println("Order was placed Successfully");
		}
		*/
		return message;
	}
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='MY CARDS']")
	public WebElement mycards;
	
	public WebElement mycards() {
		System.out.println("My Cards Button is present on Order Confirmation page\n");
		
		return mycards;
		
	}
	
	
}
