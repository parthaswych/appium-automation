package Pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OrderSummary {
	public OrderSummary(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	
		
	}
	
	@AndroidFindBy(xpath="//android.widget.CheckBox[@index='0']")
	public WebElement checkbox;
	
	public WebElement checkbox() {
		
		System.out.println("Check the terms and condition box\n");
		
		return checkbox;
	}
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='CHECKOUT']")
	public WebElement checkout;
	
	public WebElement checkout() {
		System.out.println("Clcik the Check out button on the order Summary Page\n");
		
		return checkout;
		
	}
	
	
}
