package Pageobject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

//import org.apache.commons.math3.analysis.function.Constant;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import Utilities.ExcelUtils;
import Utilities.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import Utilities.DataProviderParameter;
import Utilities.Excelfile;
import Utilities.Constants;

public class happypath extends base {

	
	
	//public static void main(String[] args) throws InterruptedException, IOException {
//	@Test(dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
//	@DataProviderParameter("FileName=" + Constants.mobilenumber + ";Sheet1")
	@DataProviderParameter("FileName=" + Constants.mobilenumber + ";Sheet=Sheet1")
	@Test(dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	public void Smoketest(String mobilenumber, String Password) throws Exception {
		AndroidDriver<AndroidElement> driver=Capabilities("apkfile","device");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//System.setProperty("org.apache.poi.util.POILogger", "org.apache.poi.util.CommonsLogger" );
		//final String file ="C:\\Users\\saapi\\Documents\\Appium\\Installation\\apache-maven-3.6.2\\bin\\SwychMobile\\src"+File.separator
		//		+"TestData.xlsx";
		//System.out.println("Path is "+file);
		//ExcelUtils.setExcelFile(file,"Data");
		
		Homepageobject hpo=new Homepageobject(driver);
		LoginPageobject login=new LoginPageobject(driver);
		LandingPageobject lpo=new LandingPageobject(driver);
		//mobilenumber = ExcelUtils.getCellData(1, 1);
		System.out.println("The mobile number is "+mobilenumber);
		System.out.println("The password is "+Password);
		// String sPassword = ExcelUtils.getCellData(1, 2);
		//hpo.mobilenumber.sendKeys("4156969775");
		//hpo.mobilenumber().sendKeys("4156969775");
		hpo.mobilenumber().clear();
		 hpo.mobilenumber().sendKeys(mobilenumber);
		hpo.getstartedbutton().click();
		//hpo.getstartedbutton.click();
		//driver.findElementByXPath("//android.widget.Button[@text='GET STARTED']").click();
		(new WebDriverWait(driver, 10)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.EditText[@text='Password']")
	            )
	        );
		
		
		//login.password.sendKeys("Swych123");
		login.password().sendKeys(Password);
		
		//login.loginbutton.click();
		login.loginbutton().click();
		
		(new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.Button[@text='Next']")
	            )
	        );
		System.out.println("Click Next Button on Instant GiftCards to INDIA\n");
		driver.findElementByXPath("//android.widget.Button[@text='Next']").click();
		(new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.Button[@text='Next']")
	            )
	        );
		System.out.println("Click Next Button on Pay with Crypto\n");
		driver.findElementByXPath("//android.widget.Button[@text='Next']").click();
		(new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.Button[@text='Next']")
	            )
	        );
		System.out.println("Click end Button on Registered with Spend Code\n");
		driver.findElementByXPath("//android.widget.Button[@text='Next']").click();
		
	    (new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.TextView[@text='NEWEST']")
	            )
	        );
	    System.out.println("Found Newest Product list in less than 30 sec\n");
	    
	    
        
	    
	    if (lpo.maycards().isDisplayed() & lpo.send().isDisplayed() & lpo.buy().isDisplayed()) {
	    	System.out.println("I can see May Cards,Send, Buy\n");
	    	
	    }else {
			System.out.println("I can not see May Cards,Send, Buy\n");
		}
		
		
		Util scrl=new Util(driver);
		scrl.scrolltotext("BROWSE ALL CARDS");
		lpo.browseallcards().click();
		
		//System.out.println("Now Scrolling down to click Browse All cards\n");
		//driver.findElementsByAndroidUIAutomator("new UiScrollable(new UiSelector())."
		//		+ "scrollIntoView(text(\"+text+\"));");
		
		(new WebDriverWait(driver, 10)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.TextView[@text='All Cards']")
	            )
	        );
		
		System.out.println("Now Scrolling down to Target card\n");
		scrl.scrolltotext("Target");
		
		AllCards acd = new AllCards(driver);
		acd.target().click();
		/*
		//driver.findElementsByAndroidUIAutomator("new UiScrollable(new UiSelector())."
		//		+ "scrollIntoView(text(\"Target\"));");
		System.out.println("Choose Target Card\n");
		driver.findElementByXPath("//android.widget.ImageView[@bounds='[546,952][1020,1243]']").click();
		*/
		
		AmountCouponPage acp= new AmountCouponPage(driver);
		System.out.println("Wait For  + button\n");
		 (new WebDriverWait(driver, 30)).until(
		            ExpectedConditions.presenceOfElementLocated(
		                By.xpath("//android.widget.ImageButton[@index='1']")  // This will work if there is a Swych Point available in your account
		            )
		        );
		 acp.plusbutton().click();
		 Thread.sleep(2000);
		 acp.dropdown().click();
		 
		 System.out.println("Wait For  maxpoints button\n");
		 (new WebDriverWait(driver, 30)).until(
		            ExpectedConditions.presenceOfElementLocated(
		                By.xpath("//android.widget.Button[@text='MAX POINTS']")  // This will work if there is a Swych Point available in your account
		            )
		        );
		 
		 acp.maxpoints().click();
		 acp.next().click();
		 /*
		 System.out.println("Click the Next button on the Select Amount Page\n");
		 driver.findElementByXPath("//android.widget.Button[@text='NEXT']").click();
		 */
		 System.out.println("Click ALLOW Button on Enable Location\n");
			(new WebDriverWait(driver, 30)).until(
		            ExpectedConditions.presenceOfElementLocated(
		                By.xpath("//android.widget.TextView[@text='ALLOW']")
		            )
		        );
			driver.findElementByXPath("//android.widget.TextView[@text='ALLOW']").click();
			Thread.sleep(5000);
			 System.out.println("Click ALLOW Button on Allow Swych window\n");
				driver.findElementByXPath("//android.widget.Button[@text='ALLOW']").click();
				
				
				
				OrderSummary osm = new OrderSummary(driver);
				
				 
				(new WebDriverWait(driver, 30)).until(
			            ExpectedConditions.presenceOfElementLocated(
			                By.xpath("//android.widget.CheckBox[@index='0']")
			            )
			        );
					if (osm.checkbox().isSelected()) {
						System.out.println("Checked the terms and condition box\n");
					}else {
						osm.checkbox().click();
					}
					osm.checkout().click();
					
					
					OrderConfirmation ocf = new OrderConfirmation(driver);
					
					(new WebDriverWait(driver, 30)).until(
				            ExpectedConditions.presenceOfElementLocated(
				                By.xpath("//android.widget.Button[@text='MY CARDS']")
				            )
				        );
					ocf.message();
					ocf.mycards();
		 
	}

}
