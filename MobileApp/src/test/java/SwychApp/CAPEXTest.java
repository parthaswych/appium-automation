package SwychApp;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.xml.xsom.impl.Ref.ContentType;

import io.restassured.http.Header;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class CAPEXTest {
	String URI="https://api.swychover.io/sandbox";
	String access_token="";
	String clinet_id ="swych_b2b_capex";
	String accountid="9185384047158163";
	String ApiKey = "d2ae25t7197f11e793ae9r2e63269v5t";
	String orderId ="";
	@Test
	 public void get_balance() 
	 {  
		//String accountid="9185384047158163";
		System.out.println("\nNow Getting Balance " );
		 System.out.println("access token used in get balance is " +access_token);
		Response response_get_balance=RestAssured.given().header("Authorization", access_token).
				header("clientId",clinet_id).
				header("ApiKey","d2ae25t7197f11e793ae9r2e63269v5t").
				header("Ocp-Apim-Subscription-Key","partha@goswych.com").
				get(URI+"/api/v1/b2b/"+accountid+"/balance");
		
		int statusCode_get_balance = response_get_balance.getStatusCode();
		 
		 String responsebody_get_balance= response_get_balance.getBody().asString();
		 System.out.println("The response from get balance is "+responsebody_get_balance);
		 System.out.println("The statuscode is "+statusCode_get_balance);
		 
		 JsonPath jsonPathEvaluator_get_balance = response_get_balance.jsonPath();
		 int status= jsonPathEvaluator_get_balance.get("statusCode");
		 //int status = Integer.parseInt(sstatus);
		 Assert.assertEquals(status, 16010600);
		/* if (status=16010600) {
			 System.out.println("The test is a success");
		 }else
		 {
			 System.out.println("The test is a fail"); 
		 }
		 */
	 }
			 
		 @Test
		 public void get_catalog() 
		 {  
			//String accountid="9185384047158163";
			 System.out.println("\nNow Getting Catalog " );
			 System.out.println("access token used in get catalog is " +access_token);
			Response response_get_balance=RestAssured.given().header("Authorization", access_token).
					header("clientId",clinet_id).
					header("ApiKey","d2ae25t7197f11e793ae9r2e63269v5t").
					header("Ocp-Apim-Subscription-Key","partha@goswych.com").
					get(URI+"/api/v1/b2b/"+accountid+"/catalog");
			
			int statusCode_get_balance = response_get_balance.getStatusCode();
			 
			 String responsebody_get_balance= response_get_balance.getBody().asString();
			 System.out.println("The response from get catalog is "+responsebody_get_balance);
			 System.out.println("The statuscode is "+statusCode_get_balance);
			 
			 JsonPath jsonPathEvaluator_get_balance = response_get_balance.jsonPath();
			 int status= jsonPathEvaluator_get_balance.get("statusCode");
			 //int status = Integer.parseInt(sstatus);
			 Assert.assertEquals(status, 16010800);
			/* if (status=16010600) {
				 System.out.println("The test is a success");
			 }else
			 {
				 System.out.println("The test is a fail"); 
			 }
			 */
				 
		 
	 }
		 
		 @Test
		 public void put_createorder() throws JSONException 
		 {  
			 
			 RestAssured.baseURI = URI;
			    RequestSpecification request = RestAssured.given();
			    System.out.println("\nNow Creating Order " );
			    request.header("Content-Type", "application/json").
			    header("Authorization",access_token).
			    header("clientId",clinet_id).
			    header("ApiKey",ApiKey).
			    header("Ocp-Apim-Subscription-Key","partha@goswych.com");
			    
			    JSONObject requestParams = new JSONObject();
			    
			    requestParams.put("accountId",accountid);
			    requestParams.put("brandId","7");
		    	requestParams.put("productId","7-0");
		    	requestParams.put("amount","10");
		    	
		    	 requestParams.put("senderFirstName","Partha");
				    requestParams.put("senderLastName","Pattanaik");
			    	requestParams.put("senderEmail","partha@goswych.com");
			    	requestParams.put("senderPhone","4156969775");
		    	
			    requestParams.put("recipientEmail","partha@goswych.com");
		    	requestParams.put("recipientPhoneNumber","4156969775");
		    	requestParams.put("notificationDeliveryMethod",0);
			    requestParams.put("giftDeliveryMode","1");
		    	requestParams.put("swychable","True");
		    	
		    	
				
		    	request.body(requestParams.toString());
				 
				
				 Response response = request.put(URI+"/api/v1/b2b/order");
						 
	
				    int statusCode = response.getStatusCode();
					 String responsebody= response.getBody().asString();
					 System.out.println("The response from create Order is "+responsebody);
					 
					 JsonPath jsonPathEvaluator = response.jsonPath();
					 int status= jsonPathEvaluator.get("statusCode");
					 //int status = Integer.parseInt(sstatus);
					 Assert.assertEquals(status, 16010900);
					 orderId= jsonPathEvaluator.get("orderId");
					 System.out.println("The order id is " +orderId);
		 
	 }
		 
		 @Test (dependsOnMethods = { "put_createorder" } )
		 public void post_confirmorder() throws JSONException 
		 {  
			 
			 RestAssured.baseURI = URI;
			    RequestSpecification request = RestAssured.given();
			    System.out.println("\nNow Confirming Order " );
			    request.header("Content-Type", "application/json").
			    header("Authorization",access_token).
			    header("clientId",clinet_id).
			    header("ApiKey",ApiKey).
			    header("Ocp-Apim-Subscription-Key","partha@goswych.com");
			    
			    JSONObject requestParams = new JSONObject();
			    
			    requestParams.put("accountId",accountid);
			    requestParams.put("orderId",orderId);
		    	
		    	
				
		    	request.body(requestParams.toString());
				 
				
				 Response response = request.post(URI+"/api/v1/b2b/order");
						 
	
				    int statusCode = response.getStatusCode();
					 String responsebody= response.getBody().asString();
					 System.out.println("The response from create Order is "+responsebody);
					 
					 JsonPath jsonPathEvaluator = response.jsonPath();
					 int status= jsonPathEvaluator.get("statusCode");
					 //int status = Integer.parseInt(sstatus);
					 Assert.assertEquals(status, 16011000);
					 
		       
	 }
	
		 @Test 
		 public void post_suggestedrate() throws JSONException 
		 {  
			 
			 RestAssured.baseURI = URI;
			    RequestSpecification request = RestAssured.given();
			    System.out.println("\nNow Doing Suggested Rate " );
			    request.header("Content-Type", "application/json").
			    header("Authorization",access_token).
			    header("clientId",clinet_id).
			    header("ApiKey",ApiKey).
			    header("Ocp-Apim-Subscription-Key","partha@goswych.com");
			    
			    JSONObject requestParams = new JSONObject();
			    
			    requestParams.put("sourceCurrency", "INR");
			    requestParams.put("targetCurrency", "USD");
		    	
		    	
				
		    	request.body(requestParams.toString());
				 
				
				 Response response = request.post(URI+"/api/v1/b2b/suggestedRate");
						 
	
				    int statusCode = response.getStatusCode();
					 String responsebody= response.getBody().asString();
					 System.out.println("The response from create Order is "+responsebody);
					 /*
					 JsonPath jsonPathEvaluator = response.jsonPath();
					 int status= jsonPathEvaluator.get("statusCode");
					 //int status = Integer.parseInt(sstatus);
					 Assert.assertEquals(status, 16011000);
					 */
		       
	 }
	 
	@DataProvider(name = "Authentication")
	
	 @BeforeTest
	 public void POST() throws JSONException
	
	 {   
		
		 
	 // Specify the base URL to the RESTful web service
		 RestAssured.baseURI = URI;
	 
	 
	 Header header = new Header("Content-Type", "application/x-www-form-urlencoded");
	 Response response = RestAssured.given().header("Content-Type", "application/x-www-form-urlencoded").
			 header("Ocp-Apim-Subscription-Key","77ad354426ab4a39a16923417829661b").
			 formParam("grant_type", "client_credentials").
			 formParam("client_id", "56a28836-6743-4249-b22b-5e1a224b1693").
			 formParam("client_secret","Y[52y.pyQXc+BLfhwy/2jkcqYXq00w5L").request().
			 formParam("resource","3a8271ef-ef89-445c-bca2-a0a66d1afc52").
			 post(URI+"/oauth2/token");
	 
     int statusCode = response.getStatusCode();
	 
	 String responsebody= response.getBody().asString();
	 System.out.println("The response from POST Authetication is "+responsebody);
	 System.out.println("The statuscode is "+statusCode);
	 
	 JsonPath jsonPathEvaluator = response.jsonPath();
	 access_token = jsonPathEvaluator.get("access_token");
	 System.out.println("access token Response " +access_token);
	 System.out.println("Done");
	
	    //String token = response.body().jsonPath().get("token");
	    //System.out.println("The responsebody is "+responsebody);
	   
	    //String access_token= responsebody.substring(185,1352);
	   // int length = access_token.length();
	    //System.out.println("Lenght is"+length);
	    //System.out.println("The responsebody is "+access_token);
	    //return token;
	    
	    
	    //Accountid = 9185384047158163
	    //Clinet id =  swych_b2b_capex
	    //API Key = d2ae25t7197f11e793ae9r2e63269v5t
	 
	 
	 }
	 
	 
	 
	
}
