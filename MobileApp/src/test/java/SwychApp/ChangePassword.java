package SwychApp;

import Pageobject.Homepageobject;
import Pageobject.LandingPageobject;
import Pageobject.LoginPageobject;
import Pageobject.MessagingListPage;
import Utilities.Constants;
import Utilities.DataProviderParameter;
import Utilities.Tools;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static Pageobject.base.Capabilities;

public class ChangePassword {

    @DataProviderParameter("FileName=" + Constants.mobilenumber + ";Sheet=Sheet1")
    @Test(dataProviderClass = Utilities.ExcelFileDataProvider.class, dataProvider = "getDataFromExcelFile")
    public void changePwd(String mobilenumber, String password) throws IOException {
        String appPackageMessaging = "com.google.android.apps.messaging"; //Nexus 5
        //String appPackageMessaging = "com.samsung.android.messaging";
        String appPackageSwych = "com.goswych.swychapp";
        String newPwd = "knn1351880";

        AndroidDriver<AndroidElement> driver = Capabilities("apkfile", "real");
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        Homepageobject hpo = new Homepageobject(driver);
        hpo.mobilenumber().clear();
        hpo.mobilenumber().sendKeys(mobilenumber);
        hpo.getstartedbutton().click();

        Tools.waitFor(50);  // wait for 50 seconds for SMS coming in
        // switch to Messaging app to read OTP
        driver.activateApp(appPackageMessaging);
        Tools.waitFor(5);  // wait for 5 seconds for the Messaging app be launched
        MessagingListPage messagingPage = new MessagingListPage(driver);
        String otp = messagingPage.getOtp();

        // switch back to Swych
        driver.activateApp(appPackageSwych);
        Tools.waitFor(5);  // wait for 5 seconds for the Swych app to be launched
        hpo.getOTPtextbox().sendKeys(otp);
        hpo.getSubmitBtn().click();

        // input password on login page and login
        LoginPageobject loginPageobject = new LoginPageobject(driver);
        loginPageobject.password.click();
        loginPageobject.password.sendKeys(password);
        driver.navigate().back();    //press device back key to hide the keyboard
        //hideKeyboardIfVisible(driver);
        loginPageobject.loginbutton.click();

        //open hamburger menu
        LandingPageobject landingPageobject = new LandingPageobject(driver);
        landingPageobject.getHamburgerBtn().click();

        //click avatar image to change password in Setting view
        landingPageobject.getAvatarImg().click();
        landingPageobject.getChangePwdClickable().click();
        landingPageobject.getOldPwdTextBox().sendKeys(password);
        landingPageobject.getnewPwdTextBox().sendKeys(newPwd);
        landingPageobject.getConfirmPwdTextBox().sendKeys(newPwd);
        landingPageobject.getPwdEditSaveBtn().click();
        String popupMessage = landingPageobject.getPopupMessage().getText();
        Assert.assertEquals(popupMessage, "Done! Your password has been updated.");
        landingPageobject.getMessageOKBtn().click();
        landingPageobject.getSignoutBtn().click();
        landingPageobject.getLogoutYes().click();

        //Change password back
        // input old password get error
        loginPageobject.password().sendKeys(password);
        loginPageobject.loginbutton().click();
        popupMessage = landingPageobject.getPopupMessage().getText();
        Assert.assertEquals(popupMessage, "Sorry, that phone number or password is not quite right. Please try again. And if you really forgot we can help you reset your password.");
        landingPageobject.getMessageOKBtn().click();
        loginPageobject.password().sendKeys(newPwd);
        //driver.navigate().back();
        Tools tools = new Tools();
        tools.hideKeyboardIfVisible(driver);
        loginPageobject.loginbutton().click();
        //click hamburger Settings to change password and then log out
        landingPageobject.getHamburgerBtn().click();
        landingPageobject.getSettingsBtn().click();
        landingPageobject.getChangePwdClickable().click();
        landingPageobject.getOldPwdTextBox().sendKeys(newPwd);
        landingPageobject.getnewPwdTextBox().sendKeys(password);
        landingPageobject.getConfirmPwdTextBox().sendKeys(password);
        landingPageobject.getPwdEditSaveBtn().click();
        landingPageobject.getMessageOKBtn().click();
        landingPageobject.getSignoutBtn().click();
        landingPageobject.getLogoutYes().click();










    }
}
