package SwychApp;

import Pageobject.Homepageobject;
import Pageobject.LandingPageobject;
import Pageobject.LoginPageobject;
import Pageobject.MessagingListPage;
import Utilities.Constants;
import Utilities.DataProviderParameter;
import Utilities.Tools;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static Pageobject.base.Capabilities;

public class LoginTakeATour {

    @DataProviderParameter("FileName=" + Constants.mobilenumber + ";Sheet=Sheet1")
    @Test(dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
    public void loginLogout(String mobilenumber, String password) throws IOException {

        String appPackageMessaging = "com.google.android.apps.messaging"; //Nexus 5
        //String appPackageMessaging = "com.samsung.android.messaging";
        String appPackageSwych = "com.goswych.swychapp";

        AndroidDriver<AndroidElement> driver = Capabilities("apkfile", "real");
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        Homepageobject hpo = new Homepageobject(driver);
        hpo.mobilenumber().clear();
        hpo.mobilenumber().sendKeys(mobilenumber);
        hpo.getstartedbutton().click();

        Tools.waitFor(50);  // wait for 50 seconds for SMS coming in
        // switch to Messaging app to read OTP
        driver.activateApp(appPackageMessaging);
        Tools.waitFor(5);  // wait for 5 seconds for the Messaging app be launched
        MessagingListPage messagingPage = new MessagingListPage(driver);
        String otp = messagingPage.getOtp();

        // switch back to Swych
        driver.activateApp(appPackageSwych);
        Tools.waitFor(5);  // wait for 5 seconds for the Swych app to be launched
        hpo.getOTPtextbox().sendKeys(otp);
        hpo.getSubmitBtn().click();

        // input password on login page and login
        LoginPageobject login = new LoginPageobject(driver);
        login.password.click();
        login.password.sendKeys(password);
        driver.navigate().back();    //press device back key to hide the keyboard
        //hideKeyboardIfVisible(driver);
        login.loginbutton.click();

        //open hamburger menu
        LandingPageobject landingPageobject = new LandingPageobject(driver);
        landingPageobject.getHamburgerBtn().click();
        // get all items on menu and identify
        MobileElement slidingSideBar = (MobileElement) landingPageobject.getSlidingSideBar();
        List<MobileElement> items = slidingSideBar.findElements(By.className("android.widget.TextView"));
        // scroll the menu list up: press on the item 6 and scroll to item 1
        Tools.scroll(driver, items.get(5), items.get(2));
        // get refreshed menu items
        items = slidingSideBar.findElements(By.className("android.widget.TextView"));
        // LOGOUT should show up and get the element
        // menu item LOGOUT
        MobileElement menuTakeATour = null;
        for (MobileElement item : items) {
            if ("TAKE A TOUR".equals(item.getText())) {
                menuTakeATour = item;  // found
                break;
            }
        }
        menuTakeATour.click();
        Tools.waitFor(15);
        //Click Next 4 times and click End
        landingPageobject.getNextBtn().click();
        landingPageobject.getNextBtn().click();
        landingPageobject.getNextBtn().click();
        landingPageobject.getNextBtn().click();
        Tools.waitFor(5);
        landingPageobject.getNextBtn().click();
        Tools.waitFor(3);

        //Go to hamburger menu and click Take A Tour again
        //open hamburger menu
        landingPageobject.getHamburgerBtn().click();
        //Tools.waitFor(10);
        // get all items on menu and identify
        slidingSideBar = (MobileElement) landingPageobject.getSlidingSideBar();
        items = slidingSideBar.findElements(By.className("android.widget.TextView"));
        // scroll the menu list up: press on the item 6 and scroll to item 1
        Tools.scroll(driver, items.get(5), items.get(2));
        // get refreshed menu items
        //items = slidingSideBar.findElements(By.className("android.widget.TextView"));
        // LOGOUT should show up and get the element
        // menu item LOGOUT
        /*MobileElement menuTakeATour = null;
        for (MobileElement item : items) {
            if ("TAKE A TOUR".equals(item.getText())) {
                menuTakeATour = item;  // found
                break;
            }
        }*/
        Tools.waitFor(2);
        menuTakeATour.click();
        Tools.waitFor(10);
        landingPageobject.getSkipBtn().click();


    }

}
