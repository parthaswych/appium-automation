package SwychApp;

import Pageobject.Homepageobject;
import Pageobject.LandingPageobject;
import Pageobject.LoginPageobject;
import Pageobject.MessagingListPage;
import Utilities.Constants;
import Utilities.DataProviderParameter;
import Utilities.Tools;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static Pageobject.base.Capabilities;
import static Utilities.Tools.waitForElement;

public class LoginUAEUser {

    private String UAEmobilenumber ;

    @DataProviderParameter("FileName=" + Constants.mobilenumber + ";Sheet=Sheet1")
    @Test(dataProviderClass = Utilities.ExcelFileDataProvider.class, dataProvider = "getDataFromExcelFile")
    public void loginAsUAEuser(String mobilenumber, String password) throws IOException {

        String appPackageMessaging = "com.google.android.apps.messaging"; //Nexus 5
        //String appPackageMessaging = "com.samsung.android.messaging";
        String appPackageSwych = "com.goswych.swychapp";
        UAEmobilenumber =  "111222666";

        AndroidDriver<AndroidElement> driver = Capabilities("apkfile", "real");
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        Homepageobject hpo = new Homepageobject(driver);
        hpo.getCountryCodeSpinner().click();
        hpo.getCountryCodeUAE().click();
        hpo.mobilenumber().clear();
        hpo.mobilenumber().sendKeys(UAEmobilenumber);
        hpo.getstartedbutton().click();

        Tools.waitFor(50);  // wait for 50 seconds for SMS coming in
        // switch to Messaging app to read OTP
        driver.activateApp(appPackageMessaging);
        Tools.waitFor(5);  // wait for 5 seconds for the Messaging app be launched
        MessagingListPage messagingPage = new MessagingListPage(driver);
        String otp = messagingPage.getOtp();

        // switch back to Swych
        driver.activateApp(appPackageSwych);
        Tools.waitFor(5);  // wait for 5 seconds for the Swych app to be launched
        hpo.getOTPtextbox().sendKeys(otp);
        hpo.getSubmitBtn().click();

        // input password on login page and login
        LoginPageobject login = new LoginPageobject(driver);
        login.password.click();
        login.password.sendKeys(password);
        driver.navigate().back();    //press device back key to hide the keyboard
        //hideKeyboardIfVisible(driver);
        login.loginbutton.click();

        //open hamburger menu
        LandingPageobject landingPageobject = new LandingPageobject(driver);
        landingPageobject.getHamburgerBtn().click();
        // get all items on menu and identify
        MobileElement slidingSideBar = (MobileElement) landingPageobject.getSlidingSideBar();
        List<MobileElement> items = slidingSideBar.findElements(By.className("android.widget.TextView"));
        // scroll the menu list up: press on the item 6 and scroll to item 1
        Tools.scroll(driver, items.get(5), items.get(2));
        // get refreshed menu items
        items = slidingSideBar.findElements(By.className("android.widget.TextView"));
        // LOGOUT should show up and get the element
        // menu item LOGOUT
        MobileElement menuLogout = null;
        for (MobileElement item : items) {
            if ("LOG OUT".equals(item.getText())) {
                menuLogout = item;  // found
                break;
            }
        }
        menuLogout.click();

        //wait for the popup and click Yes
        waitForElement(driver, landingPageobject.getLogoutYes(), 10);
        landingPageobject.getLogoutYes().click();

    }
}
