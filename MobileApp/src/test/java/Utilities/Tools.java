package Utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static io.appium.java_client.touch.offset.ElementOption.element;

public class Tools {

    public static void waitForElement(AppiumDriver driver, WebElement element, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static boolean isElementPresent(AppiumDriver driver, WebElement element){
        boolean ret = false;
        try {
            WebDriverWait wait = new WebDriverWait(driver, 0);
            wait.until(ExpectedConditions.visibilityOf(element));
            ret = true;
        } catch (Exception e) {
        }
        return ret;
    }

    // wait for a certain period of time
    public static void waitFor(int seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void scroll(AndroidDriver<AndroidElement> driver, MobileElement from, MobileElement to) {
        TouchAction ta = new TouchAction(driver);
        ta.longPress(element(from)).moveTo(element(to)).release().perform();
    }

    @AndroidFindBy(className = "UIAKeyboard")
    private AndroidElement keyboard;

    public void hideKeyboardIfVisible(AndroidDriver<AndroidElement> driver) {
        if (keyboard != null) {
            //driver.pressKey(new KeyEvent(AndroidKey.ESCAPE));
            driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        }
    }
}
