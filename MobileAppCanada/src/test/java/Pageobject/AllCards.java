package Pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AllCards {
	public AllCards(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	
		
	}
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='All Cards']")
	public WebElement allcards;
	
	public WebElement allcards() {
		System.out.println("Click Browse All cards\n");
		
		return allcards;
	}
	
	@AndroidFindBy(xpath="//android.widget.RelativeLayout[@index='2']")
	
	public WebElement golf;
	
	public WebElement golf() {
		System.out.println("Choose Golf Card\n");
		
		return golf;
	}
	
	
}
