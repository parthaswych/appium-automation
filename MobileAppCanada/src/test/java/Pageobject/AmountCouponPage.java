package Pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AmountCouponPage {
	public AmountCouponPage(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	
		
	}

	@AndroidFindBy(xpath="//android.widget.ImageButton[@index='1']") 
	public WebElement plusbutton;
	public WebElement plusbutton() { 
		System.out.println("Click + Button to add more money\n");
		
		return plusbutton;
	}
	
	@AndroidFindBy(xpath="//android.widget.TextView[@resource-id='com.goswych.swychapp:id/rewardPointsValueView']")
	public WebElement dropdown;
	public WebElement dropdown() { 
		System.out.println("Click the dropdown button\n");
		
		return dropdown;
	}
	
	
	
	
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='NEXT']")
	public WebElement next;
	
	public WebElement next() {
		System.out.println("Click the Next button on the Select Amount Page\n");
		
		return next;
	}
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='MAX POINTS']")
	public WebElement maxpoints;
	
	public WebElement maxpoints() throws InterruptedException {
		System.out.println("Click on maxpoints on the Swych Point Page\n");
		Thread.sleep(3000);
		
		return maxpoints;
	}
	
	
}
