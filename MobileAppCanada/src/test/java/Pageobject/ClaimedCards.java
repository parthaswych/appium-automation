package Pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;


public class ClaimedCards {
	public ClaimedCards(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(xpath="//android.support.v7.widget.RecyclerView[@index='1']//android.widget.RelativeLayout[@index='0']")
	public WebElement firstcard;

	@AndroidFindBy(xpath="//android.support.v7.widget.RecyclerView[@index='1']//android.widget.TextView[@index='1']")
	public WebElement value;

	public WebElement firstcard() {
		System.out.println("Click first claimed card\n");

		return firstcard;
	}

	public WebElement value() {

		return value;
	}

}
