package Pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class Hamburgmenu {
	public Hamburgmenu(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	
		
	}
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='Browse all cards']")
	public WebElement browseallcards;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='My Cards']")
	public WebElement mycards;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Send']")
	public WebElement send;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Buy']")
	public WebElement buy;

	@AndroidFindBy(id="com.goswych.swychapp:id/img_Avatar")
	public WebElement avatar;
	
	public WebElement mycards() {
		System.out.println("On the Page after login page - Find MY CARDS\n");
		return mycards;
	}
	
	public WebElement send() {
		System.out.println("On the Page after login page - Find  SEND\n");
		return send;
	}
	public WebElement buy() {
		System.out.println("On the Page after login page - Find  BUY\n");
		return buy;
	}
	public WebElement browseallcards() {
		System.out.println("Click Browse All cards\n");
		
		return browseallcards;
	}

	public WebElement avatar() {
		System.out.println("Click hamburg menu button\n");

		return avatar;
	}
	
	
}
