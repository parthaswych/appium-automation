package Pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Homepageobject {
	
	public Homepageobject(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		
	}
	
	
	@AndroidFindBy(xpath="//android.widget.EditText[@index='1']")
	public WebElement mobilenumber;
	
	
	
	@AndroidFindBy(xpath="//android.widget.Button[@text='GET STARTED']")
	public WebElement getstartedbutton;
	
	public WebElement mobilenumber() {
		System.out.println("On the Home page - Find And Enter the mobile number\n");
		return mobilenumber;
	}
	
	public WebElement getstartedbutton() {
		System.out.println("On the Home page - Find And Click Get Started\n");
		return getstartedbutton;
	}

}
