package Pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginPageobject {
	
	
	public LoginPageobject(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		
	}
	
	@AndroidFindBy(id="com.goswych.swychapp:id/edit_password")
	public WebElement password;
	
	@AndroidFindBy(id="com.goswych.swychapp:id/btnSignupLogin")
	public WebElement loginbutton;
	
	public WebElement password() {
		System.out.println("Find And Enter the Password\n");
		
		return password;
	}
	
	public WebElement loginbutton() {
		System.out.println("Find And Click Login Button\n");
		return loginbutton;
	
	}
}
