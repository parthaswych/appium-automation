package Pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class MyCards {
	public MyCards(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	
		
	}
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Claimed']")
	public WebElement claimed;
	
	public WebElement claimed() {
		System.out.println("Click Claimed tab\n");
		
		return claimed;
	}
	
}
