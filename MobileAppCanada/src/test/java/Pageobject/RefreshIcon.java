package Pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class RefreshIcon {
	public RefreshIcon(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(id = "com.goswych.swychapp:id/gift_purchase_myself_refresh")
	public WebElement refreshicon;


	public WebElement refreshicon() {
		System.out.println("Click refresh icon\n");

		return refreshicon;
	}

	@FindBy(id = "com.goswych.swychapp:id/valueView")
	public WebElement updated_value;


	public WebElement updated_value() {
		System.out.println("get updated balance\n");

		return updated_value;
	}

	@FindBy(id = "com.goswych.swychapp:id/image_back")
	public WebElement backButton;


	public WebElement backButton() {
		System.out.println("get updated balance\n");

		return backButton;
	}
	
}
