package Pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;


public class UpdateBalance {
	public UpdateBalance(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(id = "com.goswych.swychapp:id/edit_update_balance")
	public WebElement balancefield;


	public WebElement balancefield() {
		System.out.println("enter new balance\n");

		return balancefield;
	}

	@FindBy(id = "com.goswych.swychapp:id/button_update_submit")
	public WebElement submitButton;


	public WebElement submitButton() {
		System.out.println("click submit\n");

		return submitButton;
	}
	
}
