package Pageobject;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.*;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

//import com.sun.xml.fastinfoset.sax.Properties;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;


public class base {
 // @Test
	public static AndroidDriver<AndroidElement> Capabilities(String appname, String devicereal) throws IOException

	{
	
	
	Properties prop = new Properties();
	FileInputStream fis=new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\java\\global.properties");
	prop.load(fis);
    System.out.println("The apkfile used is " +prop.get("apkfile"));

	
	AndroidDriver<AndroidElement>  driver = null;
	File appDir = new File("src");
    File app = new File(appDir, (String) prop.get(appname));
	DesiredCapabilities cap = new DesiredCapabilities();
	
	
	     
	     cap.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
	     String device = (String) prop.get("device");
	     String real = (String) prop.get("real");
	     //cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Swychemulator");
	     if(devicereal.equals("real"))
	         cap.setCapability(MobileCapabilityType.DEVICE_NAME, real);
	         else
	             cap.setCapability(MobileCapabilityType.DEVICE_NAME, device);
	     
	     
	     cap.setCapability(MobileCapabilityType.DEVICE_NAME, device);
	     cap.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
	    // driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		    
	   driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
	    
	    System.out.println("Installed the app and started the Appium Server\n");
	   
	   return driver;
	   
	   
	   
	}
}




