package Pageobject;

import Utilities.Constants;
import Utilities.DataProviderParameter;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

//import org.apache.commons.math3.analysis.function.Constant;

public class happypath_send extends base {

	
	
	//public static void main(String[] args) throws InterruptedException, IOException {
//	@Test(dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
//	@DataProviderParameter("FileName=" + Constants.mobilenumber + ";Sheet1")
	@DataProviderParameter("FileName=" + Constants.mobilenumber + ";Sheet=Sheet1")
	@Test(dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	public void Smoketest(String mobilenumber, String Password) throws Exception {
		AndroidDriver<AndroidElement> driver=Capabilities("apkfile","device");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Homepageobject hpo=new Homepageobject(driver);
		LoginPageobject login=new LoginPageobject(driver);
		LandingPageobject lpo=new LandingPageobject(driver);
		(new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.EditText[@index='1']")
	            )
	        );
		//mobilenumber = ExcelUtils.getCellData(1, 1);
		System.out.println("The mobile number is "+mobilenumber);
		System.out.println("The password is "+Password);
		
		hpo.mobilenumber.clear();
		 hpo.mobilenumber.sendKeys(mobilenumber);
		hpo.getstartedbutton.click();
		//hpo.getstartedbutton.click();
		//driver.findElementByXPath("//android.widget.Button[@text='GET STARTED']").click();
		(new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.EditText[@text='Password']")
	            )
	        );
		
		
		//login.password.sendKeys("Swych123");
		login.password.sendKeys(Password);
		
		//login.loginbutton.click();
		login.loginbutton.click();
		
		(new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.Button[@text='Next']")
	            )
	        );
		System.out.println("Click Next Button on Instant GiftCards to INDIA\n");
		driver.findElementByXPath("//android.widget.Button[@text='Next']").click();
		(new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.Button[@text='Next']")
	            )
	        );
		System.out.println("Click Next Button on Pay with Crypto\n");
		driver.findElementByXPath("//android.widget.Button[@text='Next']").click();
		(new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.Button[@text='Next']")
	            )
	        );
		System.out.println("Click end Button on Registered with Spend Code\n");
		driver.findElementByXPath("//android.widget.Button[@text='Next']").click();
		
	    (new WebDriverWait(driver, 30)).until(
	            ExpectedConditions.presenceOfElementLocated(
	                By.xpath("//android.widget.TextView[@text='NEWEST']")
	            )
	        );
	    System.out.println("Found Newest Product list in less than 30 sec\n");
	    
	    
        
	    
	    if (lpo.mycards.isDisplayed() & lpo.send.isDisplayed() & lpo.buy.isDisplayed()) {
	    	System.out.println("I can see May Cards,Send, Buy\n");
	    	
	    }else {
			System.out.println("I can not see May Cards,Send, Buy\n");
		}
		
		
		//Util scrl=new Util(driver);
		lpo.send.click();

		System.out.println("Click ALLOW Button on Enable Location\n");
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.xpath("//android.widget.TextView[@text='ALLOW']")
				)
		);
		driver.findElementByXPath("//android.widget.TextView[@text='ALLOW']").click();
		System.out.println("Click ALLOW Button on Enable Location\n");
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.id("com.android.packageinstaller:id/permission_allow_button")
				)
		);
		driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();;
        sendobject snd = new sendobject(driver);
        snd.searchbutton().click();
        snd.searchbox().sendKeys("Greg");

        driver.findElementByXPath("//android.widget.RelativeLayout[@index='1']").click();
}
}

