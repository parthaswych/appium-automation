package Pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class sendobject {

	public sendobject(AppiumDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		
	}
	
	
	@AndroidFindBy(id="com.goswych.swychapp:id/search")
	public WebElement searchbutton;
	
	
	
	@AndroidFindBy(xpath="//android.widget.EditText[@text='Search']")
	public WebElement searchbox;
	
	public WebElement searchbutton() {
		System.out.println("On the Send page - Find And Click the search button\n");
		return searchbutton;
	}
	
	public WebElement searchbox() {
		System.out.println("On the Send page - Find And Click Seachbox\n");
		return searchbox;
	}

}
