package SwychApp;

import Pageobject.*;
import Utilities.Constants;
import Utilities.DataProviderParameter;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

//import org.apache.commons.math3.analysis.function.Constant;

public class Uploadavatar extends base {
	
	//public static void main(String[] args) throws InterruptedException, IOException {
//	@Test(dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
//	@DataProviderParameter("FileName=" + Constants.mobilenumber + ";Sheet1")
	@DataProviderParameter("FileName=" + Constants.mobilenumber_updatebalance + ";Sheet=Sheet1")
	@Test(dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	public void Smoketest(String mobilenumber, String Password) throws Exception {
		AndroidDriver<AndroidElement> driver = Capabilities("apkfile_johnson", "device_johnson");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		Homepageobject hpo = new Homepageobject(driver);
		LoginPageobject login = new LoginPageobject(driver);
		LandingPageobject lpo = new LandingPageobject(driver);
		Hamburgmenu hmo = new Hamburgmenu(driver);
		Setting spo = new Setting(driver);
		Photo ppo = new Photo(driver);
		MyCards mpo = new MyCards(driver);
		ClaimedCards cpo = new ClaimedCards(driver);
		RefreshIcon rpo = new RefreshIcon(driver);
		UpdateBalance upo = new UpdateBalance(driver);
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.xpath("//android.widget.EditText[@index='1']")
				)
		);
		//mobilenumber = ExcelUtils.getCellData(1, 1);
		System.out.println("The mobile number is " + mobilenumber);
		System.out.println("The password is " + Password);

		hpo.mobilenumber().clear();
		hpo.mobilenumber().sendKeys(mobilenumber);
		hpo.getstartedbutton().click();
		//hpo.getstartedbutton.click();
		//driver.findElementByXPath("//android.widget.Button[@text='GET STARTED']").click();
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.id("com.goswych.swychapp:id/edit_password")
				)
		);

		//login.password.sendKeys("Swych123");
		login.password().sendKeys(Password);

		//login.loginbutton.click();
		login.loginbutton().click();

		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.xpath("//android.widget.TextView[@text='My Cards']")
				)
		);
		System.out.println("Click My Cards tab\n");
		lpo.hamburgmenu().click();
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.id("com.goswych.swychapp:id/img_Avatar")
				)
		);

		hmo.avatar().click();
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.id("com.goswych.swychapp:id/fragment_settings_user_icon")
				)
		);

		spo.user_icon().click();
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.xpath("//android.widget.TextView[@text='Take Photo']")
				)
		);

		ppo.take_photo_btn().click();
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.id("MENUID_SHUTTER")
				)
		);

		String value = cpo.value().getText();
		String amount = value.substring(2);
		System.out.println("the amount of first claimed card is " + amount);
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.xpath("//android.support.v7.widget.RecyclerView[@index='1']//android.widget.RelativeLayout[@index='0']")
				)
		);

		cpo.firstcard().click();
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.id("com.goswych.swychapp:id/gift_purchase_myself_refresh")
				)
		);

		rpo.refreshicon().click();
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.id("com.goswych.swychapp:id/edit_update_balance")
				)
		);

		String new_amount = Integer.toString(Integer.parseInt(amount) + 10);
		upo.balancefield().sendKeys(new_amount);
		upo.submitButton().click();
		(new WebDriverWait(driver, 30)).until(
				ExpectedConditions.presenceOfElementLocated(
						By.id("com.goswych.swychapp:id/valueView")
				)
		);

		// verify if balance got updated in refreshicon page
		String updated_value_1 = rpo.updated_value().getText();
		String updated_amount_1 = updated_value_1.substring(2);
		System.out.println("the updated amount of first claimed card in refreshicon page is " + updated_amount_1);

		rpo.backButton().click();

		// verify if balance got updated in claimed page
		String updated_value_2 = cpo.value().getText();
		String updated_amount_2 = updated_value_2.substring(2);

		System.out.println("the updated amount of first claimed card in claimed page is " + updated_amount_2);

		if (updated_amount_1.equals(new_amount) & updated_amount_2.equals(new_amount)) {
			System.out.println("test passed!\n");

		} else {
			System.out.println("test failed!\n");
		}
	}

}
